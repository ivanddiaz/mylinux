#!/bin/bash
set -eu
# shellcheck disable=SC1091
source /etc/os-release
SYSTEM_NAME="$(dmidecode -s system-product-name)"

###########
# Globals
#
export LC_ALL=C
#PKG_LANG="$(locale | grep 'LANG=' | awk -F= '{print $2}' | awk -F_ '{print $1}')"
#UBUNTU_RELEASE=
SELF="$0"
NULL="null"
ARCH="$(uname -i)"
case "$ARCH" in
    x86_64|X86_64) NICE_ARCH="amd64";;
    i386|i486|i586|i686) NICE_ARCH="i386";;
    *) NICE_ARCH="$ARCH";;
esac

################
# Files & dirs
#
FILE_APT_SOURCES_LIST='/etc/apt/sources.list'
FILE_APT_CONF_UNATT_UPGRA='/etc/apt/apt.conf.d/50unattended-upgrades'
FILE_USERADD_DEFAULTS='/etc/default/useradd'
FILE_TLP_DEFAULT='/etc/tlp.conf'
DIR_BACKUP='/root/.backup'

######################
# Test first argument
#
if [ -n "$*" ]
then
    ARG1="$1"
else
    ARG1="$NULL"
fi

##############################################################################
# Functions
#

main() {
    #################
    # Main function
    # See last line of this script
    fn_execute="$(grep '()' "$SELF" | grep -Ev 'grep|^main|^null' | tr -d '(){ ' | grep -w "$ARG1" || echo "$NULL")"
    $fn_execute
}

null() {
    ######################
    # Fallback function
    #
    echo "Invalid argument. Valid arguments are:"
    echo ""
    grep '()' "$SELF" | grep -Ev 'grep|^main|^null' | sort | tr -d '(){ '
}

backup_file() {
    ###########################################
    # Internal routine for backup system files
    #
    mkdir -p "$DIR_BACKUP"
    if [ -n "$1" ]
    then
        src_file="$1"
        dst_file="$DIR_BACKUP"/"$(echo "$src_file" | tr '/' '_')"
        ls "$dst_file" >/dev/null || cp -v "$src_file" "$dst_file"
        chattr +i "$dst_file"
    fi
}

quick_start() {
    #############################
    # Install a standard system
    #
    disable_apport
    apt_main_config
    debconf_prepare_setup
    deb_ban_adobeflash
    apt_remove_thunderbird
    apt_main_installation
    release_specific_pks
    systemd_main_services
    ssh_main_config
    kernel_vm_tuning
    deb_vivaldi_install
    deb_teamviewer_install
    apt_unattended_upgrades
    debconf_reset_defaults
    deb_refresh_alternatives
    set_user_defaults
    clear
    clean_history
    print_summary
}

disable_apport() {
    ###############################
    # Ubuntu crash reporting tool
    # https://wiki.ubuntu.com/Apport
    #
    sed -i 's/enabled=1/enabled=0/g' /etc/default/apport
    systemctl disable apport
    systemctl stop apport
}

# shellcheck disable=SC2016
set_user_defaults() {
    ##################################
    # Add new users to default groups
    #
    backup_file "$FILE_USERADD_DEFAULTS"
    sed -i 's/SHELL=\/bin\/sh/SHELL=\/bin\/bash/g' "$FILE_USERADD_DEFAULTS"
    file_data=(
        '## Add new users to specific groups \n'
        '# Visit https://wiki.debian.org/SystemGroups \n'
        '@reboot root getent passwd | grep -v false | grep -i home | grep -v nologin | awk -F: +{print "gpasswd -a "$1" adm"}+ | sudo bash >/dev/null 2>&1 \n'
        '@reboot root getent passwd | grep -v false | grep -i home | grep -v nologin | awk -F: +{print "gpasswd -a "$1" cdrom"}+ | sudo bash >/dev/null 2>&1 \n'
        '@reboot root getent passwd | grep -v false | grep -i home | grep -v nologin | awk -F: +{print "gpasswd -a "$1" dip"}+ | sudo bash >/dev/null 2>&1 \n'
        '@reboot root getent passwd | grep -v false | grep -i home | grep -v nologin | awk -F: +{print "gpasswd -a "$1" lpadmin"}+ | sudo bash >/dev/null 2>&1 \n'
        '@reboot root getent passwd | grep -v false | grep -i home | grep -v nologin | awk -F: +{print "gpasswd -a "$1" lxd"}+ | sudo bash >/dev/null 2>&1 \n'
        '@reboot root getent passwd | grep -v false | grep -i home | grep -v nologin | awk -F: +{print "gpasswd -a "$1" plugdev"}+ | sudo bash >/dev/null 2>&1 \n'
        '@reboot root getent passwd | grep -v false | grep -i home | grep -v nologin | awk -F: +{print "gpasswd -a "$1" sambashare"}+ | sudo bash >/dev/null 2>&1 \n')
    echo -e "${file_data[@]}" | tr '+' \' | sed -e 's/^\ //' | sudo tee /etc/cron.d/user-defaults
}

apt_main_config() {
    ###############################
    # Apt Configuration
    # https://wiki.debian.org/Apt
    #
    systemctl stop apt-daily.timer
    systemctl stop unattended-upgrades
    backup_file "$FILE_APT_SOURCES_LIST"
    sed -i 's/[a-z][a-z].archive/archive/g'      "$FILE_APT_SOURCES_LIST"
    sed -i 's/#\ deb\ http/deb\ http/g'  "$FILE_APT_SOURCES_LIST"
    sed -i 's/^deb\ cdrom/#deb\ cdrom/g' "$FILE_APT_SOURCES_LIST"
}

debconf_prepare_setup() {
    ##################################
    # Debconf
    # Install debconf-utils for help
    # https://wiki.debian.org/debconf
    #
    systemctl restart systemd-timesyncd
    echo 'debconf debconf/priority select critical' | debconf-set-selections
    echo 'ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true' \
        | debconf-set-selections
    echo 'libdvd-pkg libdvd-pkg/post-invoke_hook-install select true' \
        | debconf-set-selections
    echo 'libdvd-pkg libdvd-pkg/post-invoke_hook-remove boolean true' \
        | debconf-set-selections
    echo 'libdvd-pkg libdvd-pkg/build select true' \
        | debconf-set-selections
    debconf-show debconf
}

apt_remove_thunderbird() {
    apt -y purge thunderbird
    apt -y autopurge
    apt -y autoremove
    apt autoclean
}

apt_main_installation() {
    ######################
    # Main Setup
    #
    pkgs_console=(aria2 curl fail2ban gdebi-core htop iotop iptraf-ng nmap \
        p7zip p7zip-full p7zip-rar powertop pwgen sysstat tlp tmux vim \
        vnstat zram-config)
        pkgs_desktop=(evolution gnome-maps)
    pkgs_media=(ffmpeg libavcodec-extra libdvd-pkg libmad0 libquicktime2 \
        libvlc5 libxvidcore4)
    pkgs_fonts=(fonts-liberation fonts-linuxlibertine fonts-texgyre \
        ttf-mscorefonts-installer)
    pkgs_extras=(qt5-style-plugins)
    apt update
    apt -y dist-upgrade
    apt -y upgrade
    apt -y --quiet install "${pkgs_console[@]}" "${pkgs_desktop[@]}" \
        "${pkgs_media[@]}" "${pkgs_fonts[@]}" "${pkgs_extras[@]}"
    dpkg-reconfigure -fnoninteractive libdvd-pkg
    dpkg -l ubuntu-desktop  && apt -y install ubuntu-restricted-addons
    dpkg -l xubuntu-desktop && apt -y install xubuntu-restricted-addons
    apt -y autoremove
    apt -y autoclean
}

debconf_reset_defaults() {
    ####################
    # Debconf reset to defaults
    #
    echo 'debconf debconf/priority select high' | debconf-set-selections
    systemctl start apt-daily.timer
    systemctl start unattended-upgrades
}

release_specific_pks () {
    sudo apt update
    if [ "$VERSION_ID" == "20.04" ]; then
        apt -y install exfat-utils
    else
        apt -y install exfatprogs
    fi
}

systemd_main_services() {
    ########################
    # Systemd services
    # https://wiki.debian.org/systemd
    #
    # VnStat config
    sed -i 's/ENABLED="false"/ENABLED="true"/g' /etc/default/sysstat
    grep -Eiv '(inter|face|lo:)' /proc/net/dev | tr  -d ':' \
        | awk '{print "vnstat -u -i " $1}'
    # TLP config
    backup_file "$FILE_TLP_DEFAULT"
    if [ -e /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor ]
    then
        current_governor="$(</sys/devices/system/cpu/cpu0/cpufreq/scaling_governor)"
        powersave_governor="conservative"
        #Test SC Fixes and remove
        #sed -i "s/^#CPU_SCALING_GOVERNOR_ON_AC.*/CPU_SCALING_GOVERNOR_ON_AC="${current_governor}"/g" "$FILE_TLP_DEFAULT"
        #sed -i "s/^#CPU_SCALING_GOVERNOR_ON_BAT.*/CPU_SCALING_GOVERNOR_ON_BAT="${powersave_governor}"/g" "$FILE_TLP_DEFAULT"
        # SC2034 SC2027
        sed -i 's/^#CPU_SCALING_GOVERNOR_ON_AC.*/CPU_SCALING_GOVERNOR_ON_AC='"${current_governor}"'/g' "$FILE_TLP_DEFAULT"
        sed -i 's/^#CPU_SCALING_GOVERNOR_ON_BAT.*/CPU_SCALING_GOVERNOR_ON_BAT='"${powersave_governor}"'/g' "$FILE_TLP_DEFAULT"
        systemctl enable  tlp
        systemctl restart tlp
    else
        echo 'TLP is not available'
        systemctl disable tlp
        systemctl stop    tlp
    fi
    # Start services
    systemctl enable sysstat
    systemctl restart sysstat
    systemctl enable  zram-config
    systemctl start   zram-config
    # Disable services
    systemctl stop    cups-browsed
    systemctl disable cups-browsed
}

ssh_main_config() {
    ########################
    # SSH Configuration
    # https://man.openbsd.org/ssh_config#ServerAliveInterval
    ls /root/.backup/_etc_ssh_ssh_config \
        || cp /etc/ssh/ssh_config /root/.backup/_etc_ssh_ssh_config
    chattr +i /root/.backup/_etc_ssh_ssh_config
    grep "ServerAliveInterval 60" /etc/ssh/ssh_config \
        || echo -e '\n'"# Keeping alive SSH sessions"'\n'"ServerAliveInterval 60" \
        | tee -a /etc/ssh/ssh_config
}

kernel_vm_tuning() {
    ########################
    # Virtual memory tuning
    # https://www.kernel.org/doc/Documentation/sysctl/vm.txt
    ls /etc/sysctl.d/20-swappiness.conf \
        || echo -e "# Tuning Virtual Memory"'\n'"vm.swappiness = 10" \
        | tee -a /etc/sysctl.d/20-swappiness.conf
    sysctl --system -p
}

deb_ban_adobeflash() {
    apt_preferences_file='/etc/apt/preferences.d/99-ban-adobeflash'
    echo '# Ban Adobe Flash Player'                                          | tee "${apt_preferences_file}"
    echo '# https://www.adobe.com/la/products/flashplayer/end-of-life.html'  | tee -a "${apt_preferences_file}"
    echo ''                                                                  | tee -a "${apt_preferences_file}"
    echo 'Package: adobe-flash-properties-gtk adobe-flash-properties-kde' \
          'adobe-flashplugin flashplugin-installer' \
          'browser-plugin-freshplayer-pepperflash pepperflashplugin-nonfree' | tee -a "${apt_preferences_file}"
    echo 'Pin: release o=*'                                                  | tee -a "${apt_preferences_file}"
    echo 'Pin-Priority: -1'                                                  | tee -a "${apt_preferences_file}"
}

deb_refresh_alternatives() {
    sudo update-alternatives --set x-www-browser /usr/bin/firefox
    sudo update-alternatives --set gnome-www-browser /usr/bin/firefox
    sudo update-alternatives --set editor /bin/nano
    sed -i 's/thunderbird.desktop/org.gnome.Evolution.desktop/g' \
    /usr/share/applications/defaults.list
}

deb_slack_install() {
    #########
    # Slack
    # https://slack.com
    # https://packagecloud.io/slacktechnologies/slack/install
    curl -L "https://packagecloud.io/slacktechnologies/slack/gpgkey" 2> /dev/null | apt-key add -
    repo_file='/etc/apt/sources.list.d/slacktechnologies_slack.list'
    echo '# This file was generated by packagecloud.io for'                             | tee "$repo_file"
    echo '# the repository at https://packagecloud.io/slacktechnologies/slack'          | tee -a "$repo_file"
    echo ''                                                                             | tee -a "$repo_file"
    echo 'deb https://packagecloud.io/slacktechnologies/slack/debian/ jessie main'      | tee -a "$repo_file"
    echo '#deb-src https://packagecloud.io/slacktechnologies/slack/debian/ jessie main' | tee -a "$repo_file"
    apt update
    apt -y install slack-desktop
}

deb_teamviewer_install() {
    ##############
    # Team Viewer
    # https://www.teamviewer.com/en/download/linux/
    # TeamViewer will be installed on amd64 and i386 systems
    # TeamViewer Host will be installed on armv7 systems
    if [ "$NICE_ARCH" == "armhf" ]
    then
        deb_pkg="teamviewer-host"
    else
        deb_pkg="teamviewer"
    fi
    if ! dpkg -l "$deb_pkg" | grep '^.i'
    then
        cd /tmp
        dpkg -l "$deb_pkg" | grep '^.i' || wget -c "https://download.teamviewer.com/download/linux/${deb_pkg}_${NICE_ARCH}.deb"
        dpkg -l "$deb_pkg" | grep '^.i' || gdebi --non-interactive "${deb_pkg}_${NICE_ARCH}.deb"
        rm -rf "${deb_pkg}_${NICE_ARCH}.deb"
        cd ~
    fi
}

deb_vivaldi_install() {
    ##############
    # Vivaldi
    # https://help.vivaldi.com/article/manual-setup-vivaldi-linux-repositories/
    if ! dpkg -l vivaldi-stable | grep '^.i'
    then
        curl -L "https://repo.vivaldi.com/archive/linux_signing_key.pub" 2> /dev/null | apt-key add -
        repo_file='/etc/apt/sources.list.d/vivaldi.list'
        echo '# Vivaldi'                             | tee "$repo_file"
        echo '# https://help.vivaldi.com/article/manual-setup-vivaldi-linux-repositories/'          | tee -a "$repo_file"
        echo ''                                                                             | tee -a "$repo_file"
        echo 'deb https://repo.vivaldi.com/archive/deb/ stable main'      | tee -a "$repo_file"
        apt update
        apt -y install vivaldi-stable
    fi
}

deb_chrome_install() {
    ##################################
    # Google Chrome
    # https://www.google.com/chrome/ for amd64 systems
    # https://www.chromium.org/Home  for other systems
    if [ "$ARCH" == "x86_64" ]
    then
        if ! dpkg -l google-chrome-stable | grep '^.i'
        then
            cd /tmp
            dpkg -l google-chrome-stable | grep '^.i' \
                || wget -c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb 
            dpkg -l google-chrome-stable | grep '^.i' \
                || gdebi --non-interactive google-chrome-stable_current_amd64.deb
            rm -f google-chrome-stable_current_amd64.deb
            cd ~
            mkdir -p /etc/skel/.config/google-chrome/Default/
            echo '{"browser":{"custom_chrome_frame":false}}' > /etc/skel/.config/google-chrome/Default/Preferences
        fi
    else
        snap install chromium
    fi
}

deb_insync_install() {
    apt-key list| grep -i insync \
        || apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C \
        || apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys ACCAF35C
    if [ ! -e /etc/apt/sources.list.d/insync.list ]; then
        echo "# https://www.insynchq.com/downloads" | tee /etc/apt/sources.list.d/insync.list
        echo "deb http://apt.insync.io/$ID $VERSION_CODENAME non-free contrib"  | tee -a /etc/apt/sources.list.d/insync.list
    fi
    sudo apt update
    dpkg -l ubuntu-desktop  && apt -y install insync insync-nautilus
    dpkg -l xubuntu-desktop && apt -y install insync insync-thunar
}

deb_megasync_install() {
wget -qO - "https://mega.nz/linux/MEGAsync/x${NAME}_${VERSION_ID}/Release.key" | sudo apt-key add -
    if [ ! -e /etc/apt/sources.list.d/megasync.list ]; then
        echo "deb http://mega.nz/linux/MEGAsync/x${NAME}_${VERSION_ID}/ ./"  | tee -a /etc/apt/sources.list.d/megasync.list
    fi
    sudo apt update
    dpkg -l ubuntu-desktop  && apt -y install megasync nautilus-megasync
    dpkg -l xubuntu-desktop && apt -y install megasync thunar-megasync
}

deb_libvirt_install() {
    sudo apt -y install virt-manager
    echo [User]+SystemAccount=true | tr '+' '\n' | sudo tee /var/lib/AccountsService/users/libvirt-qemu
    systemctl restart accounts-daemon
}

# shellcheck disable=SC2016
apt_unattended_upgrades() {
    ###########################
    # Apt Unattended Upgrades
    # https://wiki.debian.org/UnattendedUpgrades
    #
    echo 'unattended-upgrades unattended-upgrades/enable_auto_updates select true' | debconf-set-selections
    dpkg-reconfigure -p critical unattended-upgrades
    backup_file "$FILE_APT_CONF_UNATT_UPGRA"
    chattr +i /root/.backup/_etc_apt_apt.conf.d_50unattended-upgrades
    sed -i "$(grep -n '}-security'  "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//)"'s/\/\///g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i "$(grep -n '}-updates'   "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//)"'s/\/\///g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i "$(grep -n '}-proposed'  "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//)"'s/\/\///g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i "$(grep -n '}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//)"'s/\/\///g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/Unattended-Upgrade::AutoFixInterruptedDpkg\ "false";/Unattended-Upgrade::AutoFixInterruptedDpkg\ "true";/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/\/\/Unattended-Upgrade::AutoFixInterruptedDpkg/Unattended-Upgrade::AutoFixInterruptedDpkg/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/Unattended-Upgrade::MinimalSteps\ "false";/Unattended-Upgrade::MinimalSteps\ "true";/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/\/\/Unattended-Upgrade::MinimalSteps/Unattended-Upgrade::MinimalSteps/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/Unattended-Upgrade::Remove-Unused-Kernel-Packages\ "false";/Unattended-Upgrade::Remove-Unused-Kernel-Packages\ "true";/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/\/\/Unattended-Upgrade::Remove-Unused-Kernel-Packages/Unattended-Upgrade::Remove-Unused-Kernel-Packages/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/Unattended-Upgrade::Remove-New-Unused-Dependencies\ "false";/Unattended-Upgrade::Remove-New-Unused-Dependencies\ "true";/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/\/\/Unattended-Upgrade::Remove-New-Unused-Dependencies/Unattended-Upgrade::Remove-New-Unused-Dependencies/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/Unattended-Upgrade::Remove-Unused-Dependencies\ "false";/Unattended-Upgrade::Remove-Unused-Dependencies\ "true";/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/\/\/Unattended-Upgrade::Remove-Unused-Dependencies/Unattended-Upgrade::Remove-Unused-Dependencies/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/Unattended-Upgrade::OnlyOnACPower\ "false";/Unattended-Upgrade::OnlyOnACPower\ "true";/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/\/\/Unattended-Upgrade::OnlyOnACPower/Unattended-Upgrade::OnlyOnACPower/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/Unattended-Upgrade::Skip-Updates-On-Metered-Connections\ "false";/Unattended-Upgrade::Skip-Updates-On-Metered-Connections\ "true";/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/\/\/Unattended-Upgrade::Skip-Updates-On-Metered-Connections/Unattended-Upgrade::Skip-Updates-On-Metered-Connections/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/Acquire::http::Dl-Limit\ "70";/Acquire::http::Dl-Limit\ "256";/g' "$FILE_APT_CONF_UNATT_UPGRA"
    sed -i 's/\/\/Acquire::http::Dl-Limit/Acquire::http::Dl-Limit/g' "$FILE_APT_CONF_UNATT_UPGRA"
    grep 'Vivaldi' "$FILE_APT_CONF_UNATT_UPGRA" || sed -i "$(($(grep -n '${distro_id}:${distro_codename}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//) + 1))i\\\t\"Vivaldi\ Technologies:stable\"\;" "$FILE_APT_CONF_UNATT_UPGRA"
    grep 'TeamViewer GmbH:stable' "$FILE_APT_CONF_UNATT_UPGRA" || sed -i "$(($(grep -n '${distro_id}:${distro_codename}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//) + 1))i\\\t\"TeamViewer\ GmbH:stable\"\;" "$FILE_APT_CONF_UNATT_UPGRA"
    grep 'slacktechnologies'  "$FILE_APT_CONF_UNATT_UPGRA" || sed -i "$(($(grep -n '${distro_id}:${distro_codename}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//) + 1))i\\\t\"packagecloud.io/slacktechnologies/slack:jessie\"\;" "$FILE_APT_CONF_UNATT_UPGRA"
    grep 'Oracle'  "$FILE_APT_CONF_UNATT_UPGRA" || sed -i "$(($(grep -n '${distro_id}:${distro_codename}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//) + 1))i\\\t\"Oracle\ Corporation.:$UBUNTU_CODENAME\"\;" "$FILE_APT_CONF_UNATT_UPGRA"
    grep 'Insynchq'  "$FILE_APT_CONF_UNATT_UPGRA" || sed -i "$(($(grep -n '${distro_id}:${distro_codename}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//) + 1))i\\\t\"Insynchq\\\,\ Inc.:$UBUNTU_CODENAME\"\;" "$FILE_APT_CONF_UNATT_UPGRA"
    grep 'Google'  "$FILE_APT_CONF_UNATT_UPGRA" || sed -i "$(($(grep -n '${distro_id}:${distro_codename}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//) + 1))i\\\t\"Google\ LLC:stable\"\;" "$FILE_APT_CONF_UNATT_UPGRA"
    grep 'Docker'  "$FILE_APT_CONF_UNATT_UPGRA" || sed -i "$(($(grep -n '${distro_id}:${distro_codename}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//) + 1))i\\\t\"Docker:$UBUNTU_CODENAME\"\;" "$FILE_APT_CONF_UNATT_UPGRA"
    grep 'Canonical'  "$FILE_APT_CONF_UNATT_UPGRA" || sed -i "$(($(grep -n '${distro_id}:${distro_codename}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//) + 1))i\\\t\"Canonical:$UBUNTU_CODENAME\"\;" "$FILE_APT_CONF_UNATT_UPGRA"
    grep 'MEGA'  "$FILE_APT_CONF_UNATT_UPGRA" || sed -i "$(($(grep -n '${distro_id}:${distro_codename}-backports' "$FILE_APT_CONF_UNATT_UPGRA" | sed -e s/:.*//) + 1))i\\\t\"MEGA:$UBUNTU_CODENAME\"\;" "$FILE_APT_CONF_UNATT_UPGRA"
}

clean_history() {
    cd /home
    for USER in *; do
        rm -f "$USER"/.bash_history
    done
    cd
    sudo rm -f /root/.bash_history
}

print_summary() {
    ############################
    # Show an execution resume
    #
    echo '#############'
    echo '## SUMMARY ##'
    echo ''
    echo '## Debconf:'
    debconf-show debconf
    echo ''
    echo '## VnStat:'
    vnstat
    echo ''
    echo '## ZRam:'
    cat /proc/swaps
    echo ''
    echo '## SysStat:'
    sar -q | tail -10 | grep -v '^$'
    echo ''
    echo '## Kernel VM:'
    sysctl -a 2>/dev/null | grep vm. | grep -E '(swap|dirty.*ratio)'
    echo ''
    echo '## TLP:'
    systemctl status tlp
    echo ''
    echo '## TCP/UDP Ports:'
    ss -ltu
    echo ''
    echo 'EOF'
    echo ''
}

# Main trap
main
