# myLinux

Short link: <https://tinyurl.com/y4al5ve3>

## Quick Install

```bash
START=$(mktemp); wget -O "${START}" https://gitlab.com/ivanddiaz/mylinux/-/raw/master/start.sh; sudo bash "${START}" quick_start; unset START; history -c
```

## Add-On packages

### Chrome / Chromium

```bash
START=$(mktemp); wget -O "${START}" https://gitlab.com/ivanddiaz/mylinux/-/raw/master/start.sh; sudo bash "${START}" deb_chrome_install; unset START; history -c
```

### Libvirt / KVM

```bash
START=$(mktemp); wget -O "${START}" https://gitlab.com/ivanddiaz/mylinux/-/raw/master/start.sh; sudo bash "${START}" deb_libvirt_install; unset START; history -c
```

### MEGA

```bash
START=$(mktemp); wget -O "${START}" https://gitlab.com/ivanddiaz/mylinux/-/raw/master/start.sh; sudo bash "${START}" deb_libvirt_install; unset START; history -c
```

### Insync

```bash
START=$(mktemp); wget -O "${START}" https://gitlab.com/ivanddiaz/mylinux/-/raw/master/start.sh; sudo bash "${START}" deb_insync_install; unset START; history -c
```

### Slack

```bash
START=$(mktemp); wget -O "${START}" https://gitlab.com/ivanddiaz/mylinux/-/raw/master/start.sh; sudo bash "${START}" deb_slack_install; unset START; history -c
```

### Microsoft teams

```bash
START=$(mktemp); wget -O "${START}" https://gitlab.com/ivanddiaz/mylinux/-/raw/master/start.sh; sudo bash "${START}" deb_teams_install; unset START; history -c
```

### Wallpapers

```bash
START=$(mktemp); wget -O "${START}" https://gitlab.com/ivanddiaz/mylinux/-/raw/master/start.sh; sudo bash "${START}" misc_install_wallpapers; unset START; history -c
```
